# Memorization Game

A simple card memorization game. Match the cards.

## Get started

```
$ npm install
$ npm start
```
