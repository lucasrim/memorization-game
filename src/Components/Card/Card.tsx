import React, { FC } from 'react';
import classNames from 'classnames';
import { Asset } from '../Asset';
import styles from './styles.module.css';

type CardProps = {
  value: number;
  isFlipped: boolean;
  isMatched: boolean;
  index: number;
  paused: boolean;
  onCardClick: (index: number) => void;
};

export const Card: FC<CardProps> = (props) => {
  const { value, index, isFlipped, onCardClick, isMatched, paused } = props;
  const cardStyles = classNames(styles.card, {
    [styles.cardFlipped]: isFlipped || isMatched,
    [styles.paused]: paused,
  });

  return (
    <div
      className={cardStyles}
      onClick={() => (paused ? {} : onCardClick(index))}
    >
      {isFlipped || isMatched ? (
        <span>{value}</span>
      ) : (
        <Asset variant="stripes" />
      )}
    </div>
  );
};
