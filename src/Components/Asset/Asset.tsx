import * as React from 'react';
import VariantToSvg from './VariantToSvg';

export type Variant = keyof typeof VariantToSvg;

export const isAssetVariant = (variant: string): variant is Variant => {
  return VariantToSvg.hasOwnProperty(variant);
};

interface Props {
  variant: Variant;
}

const Asset: React.SFC<Props> = ({ variant }) => VariantToSvg[variant];

export { Asset };
