import * as React from 'react';

export default (
  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
    <defs>
      <pattern
        id="pattern_L9ATL"
        patternUnits="userSpaceOnUse"
        width="11"
        height="11"
        patternTransform="rotate(45)"
      >
        <line x1="0" y="0" x2="0" y2="11" stroke="#F98302" strokeWidth="5" />
      </pattern>
    </defs>
    <rect width="100%" height="100%" fill="url(#pattern_L9ATL)" opacity="1" />
  </svg>
);
