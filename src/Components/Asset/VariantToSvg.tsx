import * as React from 'react';
import stripes from './svgs/stripes.svg';

const variantMapping: {
  stripes: React.ReactElement<unknown>;
} = {
  stripes,
};

export default variantMapping;
