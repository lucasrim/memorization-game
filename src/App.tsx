import React, { useReducer, FC } from 'react';
import { initialState, reducer } from './reducer';
import { Card } from './Components';
import styles from './styles.module.css';

const delay = (time = 600) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

const App: FC = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const pauseGame = async () => {
    const flippedCards = state.deck.filter((card) => card.isFlipped);
    if (flippedCards.length > 0) {
      await delay();
      dispatch({ type: 'unflipCards' });
    }
  };
  const onCardClick = async (index: number) => {
    dispatch({ type: 'flipCard', index });
    dispatch({ type: 'checkMatch', index });
    dispatch({ type: 'checkWin' });
    await pauseGame();
  };
  const onResetClick = () => {
    dispatch({ type: 'reset' });
  };
  const onIncrementVariantsClick = () => {
    dispatch({ type: 'incrementVariants' });
    dispatch({ type: 'reset' });
  };
  const onDecrementVariantsClick = () => {
    dispatch({ type: 'decrementVariants' });
    dispatch({ type: 'reset' });
  };

  return (
    <div className={styles.app}>
      <div className={styles.buttonContainer}>
        <button className={styles.button} onClick={onResetClick}>
          Reset
        </button>
        <button className={styles.button} onClick={onIncrementVariantsClick}>
          + Cards
        </button>
        <button
          className={styles.button}
          onClick={onDecrementVariantsClick}
          disabled={state.variants < 2}
        >
          - Cards
        </button>
      </div>
      {state.won && <h1>You Win!</h1>}
      <div className={styles.deck}>
        {state.deck.map((card, index) => (
          <Card
            key={index}
            onCardClick={onCardClick}
            paused={state.paused}
            index={index}
            {...card}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
