export type CardType = {
  value: number;
  isFlipped: boolean;
  isMatched: boolean;
};

export type StateType = {
  variants: number;
  deck: CardType[];
  paused: boolean;
  won: boolean;
};

function createDeck(variants: number): CardType[] {
  return [
    ...Array.from(new Array(variants), (_x, i) => {
      return { value: i + 1, isFlipped: false, isMatched: false };
    }),
    ...Array.from(new Array(variants), (_x, i) => {
      return { value: i + 1, isFlipped: false, isMatched: false };
    }),
  ];
}

function resetFlippedCards(deck: CardType[]): CardType[] {
  return deck.map((card) => {
    return {
      ...card,
      isFlipped: false,
    };
  });
}

function flipCard(deck: CardType[], index: number): CardType[] {
  return Object.assign([], deck, {
    [index]: {
      ...deck[index],
      isFlipped: true,
    },
  });
}

function matchCards(
  deck: CardType[],
  firstIndex: number,
  secondIndex: number
): CardType[] {
  return Object.assign([], deck, {
    [firstIndex]: {
      ...deck[firstIndex],
      isMatched: true,
    },
    [secondIndex]: {
      ...deck[secondIndex],
      isMatched: true,
    },
  });
}

function findFlippedIndexes(deck: CardType[]): number[] {
  return deck.reduce(
    (accumulator, card, index) =>
      card.isFlipped ? accumulator.concat(index) : accumulator,
    [] as number[]
  );
}

function shuffleDeck(deck: CardType[]): CardType[] {
  return deck.sort(() => Math.random() - 0.5);
}

export const initialState: StateType = {
  variants: 12,
  deck: shuffleDeck(createDeck(12)),
  paused: false,
  won: false,
};

export type ActionType =
  | {
      type: 'flipCard';
      index: number;
    }
  | {
      type: 'checkMatch';
      index: number;
    }
  | {
      type: 'unflipCards';
    }
  | {
      type: 'reset';
    }
  | {
      type: 'incrementVariants';
    }
  | { type: 'decrementVariants' }
  | { type: 'checkWin' };

export function reducer(state: StateType, action: ActionType) {
  const flippedCardIndexes = findFlippedIndexes(state.deck);
  const flippedCards: CardType[] = state.deck.filter((card) => card.isFlipped);
  switch (action.type) {
    case 'reset':
      return {
        ...state,
        deck: shuffleDeck(createDeck(state.variants)),
        won: false,
      };
    case 'incrementVariants':
      return {
        ...state,
        variants: state.variants + 1,
      };
    case 'decrementVariants':
      return {
        ...state,
        variants: state.variants - 1,
      };
    case 'flipCard':
      return {
        ...state,
        deck: flipCard(state.deck, action.index),
      };
    case 'checkMatch':
      if (flippedCards.length === 1)
        return {
          ...state,
        };

      const firstCard = state.deck[flippedCardIndexes[0]];
      const secondCard = state.deck[flippedCardIndexes[1]];

      if (firstCard.value === secondCard.value)
        return {
          ...state,
          deck: matchCards(
            state.deck,
            flippedCardIndexes[0],
            flippedCardIndexes[1]
          ),
          paused: true,
        };

      return {
        ...state,
        paused: true,
      };
    case 'unflipCards':
      if (flippedCards.length > 1)
        return {
          ...state,
          deck: resetFlippedCards(state.deck),
          paused: false,
        };

      return { ...state };
    case 'checkWin':
      if (
        state.deck.filter((card) => card.isMatched).length ===
        state.variants * 2
      ) {
        return {
          ...state,
          won: true,
        };
      }

      return {
        ...state,
      };
    default:
      return {
        ...state,
      };
  }
}
